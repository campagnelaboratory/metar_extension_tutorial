<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e53de41e-3f09-4ea1-93c9-8712be067a83(your.domain.heatmap2.generator.template.main@generator)">
  <persistence version="9" />
  <languages>
    <use id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext" version="-1" />
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="-1" />
    <use id="8f241757-0a4b-4332-b1b7-5a58efeb6822" name="your.domain.heatmap2" version="-1" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="-1" />
    <use id="901f5cf3-dc77-4c1e-bc5a-6382baee28b4" name="org.campagnelab.textoutput" version="-1" />
    <use id="32f503e8-061b-451e-bcb0-fef56aa05eb9" name="org.campagnelab.metar.inspect" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="v8sa" ref="r:db1b133e-9a0f-4319-b384-413408eb1729(org.campagnelab.metar.tables.behavior)" />
    <import index="1mjk" ref="r:72b0aa20-b681-4aef-ad30-bb23b1f4b98c(org.campagnelab.metar.code.generator.helpers)" />
    <import index="jrxw" ref="r:9f2bbfbf-f8b7-4b3b-92b1-b6a0e9642c10(org.campagnelab.metar.tables.structure)" />
    <import index="6vq7" ref="r:32a8a404-dfb4-4267-85ee-151bfcea0367(your.domain.heatmap2.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
    </language>
    <language id="901f5cf3-dc77-4c1e-bc5a-6382baee28b4" name="org.campagnelab.textoutput">
      <concept id="5039633819242576787" name="org.campagnelab.textoutput.structure.Lines" flags="ng" index="2G3XJi">
        <child id="5039633819242576854" name="lines" index="2G3XIn" />
      </concept>
      <concept id="5493669862519709805" name="org.campagnelab.textoutput.structure.Line" flags="ng" index="1gZcZf">
        <property id="5493669862519718600" name="text" index="1gZaPE" />
        <child id="1680136183140337477" name="phrases" index="3_3kQL" />
      </concept>
      <concept id="1680136183140337486" name="org.campagnelab.textoutput.structure.Phrase" flags="ng" index="3_3kQU">
        <property id="1680136183140337487" name="text" index="3_3kQV" />
      </concept>
    </language>
    <language id="32f503e8-061b-451e-bcb0-fef56aa05eb9" name="org.campagnelab.metar.inspect">
      <concept id="8969925079115431553" name="org.campagnelab.metar.inspect.structure.TryAndReport" flags="ng" index="3eWmRk">
        <property id="2742007948304393655" name="enabled" index="2BXFg4" />
        <property id="8969925079115431616" name="nodeId" index="3eWmQl" />
        <child id="8969925079115431619" name="try" index="3eWmQm" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="propertyName" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4QWKC3BlMh6">
    <property role="TrG5h" value="main" />
    <node concept="3aamgX" id="5c0KNUygeTQ" role="3acgRq">
      <ref role="30HIoZ" to="6vq7:4QWKC3BlN_S" resolve="Heatmap2" />
      <node concept="j$656" id="7c9ftft9en$" role="1lVwrX">
        <ref role="v9R2y" node="5c0KNUygobM" resolve="reduce_Heatmap2" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="5c0KNUygobM">
    <property role="TrG5h" value="reduce_Heatmap2" />
    <ref role="3gUMe" to="6vq7:4QWKC3BlN_S" resolve="Heatmap2" />
    <node concept="2G3XJi" id="5c0KNUygobR" role="13RCb5">
      <property role="1gZaPE" value="" />
      <node concept="2G3XJi" id="5c0KNUygJMx" role="2G3XIn">
        <property role="1gZaPE" value="" />
        <node concept="2G3XJi" id="5c0KNUygJNh" role="2G3XIn">
          <property role="1gZaPE" value="" />
          <node concept="1gZcZf" id="5c0KNUygJNv" role="2G3XIn">
            <property role="1gZaPE" value="" />
            <node concept="3_3kQU" id="5c0KNUygR6B" role="3_3kQL">
              <property role="3_3kQV" value="plot_" />
            </node>
            <node concept="3_3kQU" id="5c0KNUygR6C" role="3_3kQL">
              <property role="3_3kQV" value="id" />
              <node concept="17Uvod" id="5c0KNUygR6K" role="lGtFl">
                <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                <property role="2qtEX9" value="text" />
                <node concept="3zFVjK" id="5c0KNUygR6L" role="3zH0cK">
                  <node concept="3clFbS" id="5c0KNUygR6M" role="2VODD2">
                    <node concept="3clFbF" id="5c0KNUygRbN" role="3cqZAp">
                      <node concept="2OqwBi" id="5c0KNUygRgB" role="3clFbG">
                        <node concept="30H73N" id="5c0KNUygRbM" role="2Oq$k0" />
                        <node concept="3TrcHB" id="7c9ftft63Z$" role="2OqNvi">
                          <ref role="3TsBF5" to="jrxw:7LvyiX4miiD" resolve="id" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3_3kQU" id="5c0KNUygR6D" role="3_3kQL">
              <property role="3_3kQV" value="=function(t){" />
            </node>
            <node concept="3_3kQU" id="5c0KNUygJNS" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
            <node concept="3_3kQU" id="5c0KNUygJNB" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
            <node concept="3_3kQU" id="5c0KNUygJNw" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
          </node>
          <node concept="3eWmRk" id="5c0KNUygJOb" role="2G3XIn">
            <property role="1gZaPE" value="" />
            <property role="2BXFg4" value="true" />
            <property role="3eWmQl" value="id" />
            <node concept="1gZcZf" id="5c0KNUygJOc" role="2G3XIn">
              <property role="1gZaPE" value="" />
            </node>
            <node concept="1gZcZf" id="5c0KNUygJOf" role="2G3XIn">
              <property role="1gZaPE" value="" />
              <node concept="3_3kQU" id="5c0KNUygJOg" role="3_3kQL">
                <property role="3_3kQV" value="" />
              </node>
            </node>
            <node concept="3_3kQU" id="5c0KNUygJOi" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
            <node concept="2G3XJi" id="5c0KNUygJOk" role="3eWmQm">
              <property role="1gZaPE" value="" />
              <node concept="1gZcZf" id="5c0KNUygJOl" role="2G3XIn">
                <property role="1gZaPE" value="heatmap.2(as.matrix(t))" />
              </node>
              <node concept="3_3kQU" id="5c0KNUygJOr" role="3_3kQL">
                <property role="3_3kQV" value="" />
              </node>
            </node>
            <node concept="17Uvod" id="5c0KNUygJQY" role="lGtFl">
              <property role="P4ACc" value="32f503e8-061b-451e-bcb0-fef56aa05eb9/8969925079115431553/8969925079115431616" />
              <property role="2qtEX9" value="nodeId" />
              <node concept="3zFVjK" id="5c0KNUygJQZ" role="3zH0cK">
                <node concept="3clFbS" id="5c0KNUygJR0" role="2VODD2">
                  <node concept="3clFbF" id="5c0KNUygJW1" role="3cqZAp">
                    <node concept="2OqwBi" id="5c0KNUygK0P" role="3clFbG">
                      <node concept="30H73N" id="5c0KNUygJW0" role="2Oq$k0" />
                      <node concept="2qgKlT" id="5c0KNUygKHM" role="2OqNvi">
                        <ref role="37wK5l" to="v8sa:2AV3DmgHDX5" resolve="id" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="17Uvod" id="7c9ftft76hy" role="lGtFl">
              <property role="P4ACc" value="32f503e8-061b-451e-bcb0-fef56aa05eb9/8969925079115431553/2742007948304393655" />
              <property role="2qtEX9" value="enabled" />
              <node concept="3zFVjK" id="7c9ftft76hz" role="3zH0cK">
                <node concept="3clFbS" id="7c9ftft76h$" role="2VODD2">
                  <node concept="3clFbF" id="2od$re233Qr" role="3cqZAp">
                    <node concept="2OqwBi" id="2od$re233WE" role="3clFbG">
                      <node concept="30H73N" id="2od$re233Qq" role="2Oq$k0" />
                      <node concept="2qgKlT" id="2od$re234Uc" role="2OqNvi">
                        <ref role="37wK5l" to="v8sa:2od$re20jdz" resolve="errorCatchingEnabled" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1gZcZf" id="5c0KNUygJOK" role="2G3XIn">
            <property role="1gZaPE" value="}" />
            <node concept="3_3kQU" id="5c0KNUygJPC" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
            <node concept="3_3kQU" id="5c0KNUygJOL" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
          </node>
          <node concept="3eWmRk" id="2AV3DmgQAGh" role="2G3XIn">
            <property role="1gZaPE" value="" />
            <property role="3eWmQl" value="nodeId" />
            <node concept="1gZcZf" id="2AV3DmgQAGi" role="2G3XIn">
              <property role="1gZaPE" value="" />
            </node>
            <node concept="3_3kQU" id="2AV3DmgQAGm" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
            <node concept="1gZcZf" id="2AV3DmgQAGo" role="2G3XIn">
              <property role="1gZaPE" value="" />
              <node concept="3_3kQU" id="2AV3DmgQAGp" role="3_3kQL">
                <property role="3_3kQV" value="" />
              </node>
            </node>
            <node concept="2G3XJi" id="2AV3DmgQAGr" role="3eWmQm">
              <property role="1gZaPE" value="" />
              <node concept="1gZcZf" id="4WAdMLdio0T" role="2G3XIn">
                <property role="1gZaPE" value="" />
                <node concept="3_3kQU" id="4WAdMLdio0U" role="3_3kQL">
                  <property role="3_3kQV" value="png(file=&quot;" />
                </node>
                <node concept="3_3kQU" id="4WAdMLdio0V" role="3_3kQL">
                  <property role="3_3kQV" value="plot.png" />
                  <node concept="17Uvod" id="4WAdMLdio0W" role="lGtFl">
                    <property role="2qtEX9" value="text" />
                    <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                    <node concept="3zFVjK" id="4WAdMLdio0X" role="3zH0cK">
                      <node concept="3clFbS" id="4WAdMLdio0Y" role="2VODD2">
                        <node concept="3clFbF" id="4RSqyaA7BmM" role="3cqZAp">
                          <node concept="2OqwBi" id="4RSqyaA7Cf6" role="3clFbG">
                            <node concept="2ShNRf" id="4RSqyaA7BmI" role="2Oq$k0">
                              <node concept="1pGfFk" id="4RSqyaA7BLK" role="2ShVmc">
                                <ref role="37wK5l" to="1mjk:4RSqyaA71W6" resolve="RPath" />
                                <node concept="2OqwBi" id="4RSqyaA7BZO" role="37wK5m">
                                  <node concept="2OqwBi" id="4RSqyaA7BZP" role="2Oq$k0">
                                    <node concept="30H73N" id="4RSqyaA7BZQ" role="2Oq$k0" />
                                    <node concept="3TrEf2" id="5c0KNUygUNL" role="2OqNvi">
                                      <ref role="3Tt5mk" to="6vq7:4QWKC3BlYri" />
                                    </node>
                                  </node>
                                  <node concept="2qgKlT" id="4RSqyaA7BZS" role="2OqNvi">
                                    <ref role="37wK5l" to="v8sa:6X05ubabEHv" resolve="getPath" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="4RSqyaA7Dme" role="2OqNvi">
                              <ref role="37wK5l" to="1mjk:4RSqyaA7akQ" resolve="toString" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3_3kQU" id="4WAdMLdio15" role="3_3kQL">
                  <property role="3_3kQV" value="&quot;, width=" />
                </node>
                <node concept="3_3kQU" id="4WAdMLdio16" role="3_3kQL">
                  <property role="3_3kQV" value="w" />
                  <node concept="17Uvod" id="4WAdMLdio17" role="lGtFl">
                    <property role="2qtEX9" value="text" />
                    <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                    <node concept="3zFVjK" id="4WAdMLdio18" role="3zH0cK">
                      <node concept="3clFbS" id="4WAdMLdio19" role="2VODD2">
                        <node concept="3clFbF" id="4WAdMLdio1a" role="3cqZAp">
                          <node concept="2YIFZM" id="4WAdMLdio1b" role="3clFbG">
                            <ref role="1Pybhc" to="e2lb:~Integer" resolve="Integer" />
                            <ref role="37wK5l" to="e2lb:~Integer.toString(int):java.lang.String" resolve="toString" />
                            <node concept="2OqwBi" id="4WAdMLdio1c" role="37wK5m">
                              <node concept="2OqwBi" id="32mm940NOly" role="2Oq$k0">
                                <node concept="30H73N" id="4WAdMLdio1e" role="2Oq$k0" />
                                <node concept="3TrEf2" id="5c0KNUygV6_" role="2OqNvi">
                                  <ref role="3Tt5mk" to="6vq7:4QWKC3BlYri" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="32mm940uBty" role="2OqNvi">
                                <ref role="37wK5l" to="v8sa:32mm940cTd2" resolve="getWidth" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3_3kQU" id="4WAdMLdio1h" role="3_3kQL">
                  <property role="3_3kQV" value=", height=" />
                </node>
                <node concept="3_3kQU" id="4WAdMLdio1i" role="3_3kQL">
                  <property role="3_3kQV" value="h" />
                  <node concept="17Uvod" id="4WAdMLdio1j" role="lGtFl">
                    <property role="2qtEX9" value="text" />
                    <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                    <node concept="3zFVjK" id="4WAdMLdio1k" role="3zH0cK">
                      <node concept="3clFbS" id="4WAdMLdio1l" role="2VODD2">
                        <node concept="3clFbF" id="4WAdMLdio1m" role="3cqZAp">
                          <node concept="2YIFZM" id="4WAdMLdio1n" role="3clFbG">
                            <ref role="1Pybhc" to="e2lb:~Integer" resolve="Integer" />
                            <ref role="37wK5l" to="e2lb:~Integer.toString(int):java.lang.String" resolve="toString" />
                            <node concept="2OqwBi" id="32mm940v2hp" role="37wK5m">
                              <node concept="2OqwBi" id="32mm940NPl2" role="2Oq$k0">
                                <node concept="30H73N" id="32mm940v2av" role="2Oq$k0" />
                                <node concept="3TrEf2" id="5c0KNUygVp6" role="2OqNvi">
                                  <ref role="3Tt5mk" to="6vq7:4QWKC3BlYri" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="32mm940v2VX" role="2OqNvi">
                                <ref role="37wK5l" to="v8sa:32mm940d1rP" resolve="getHeight" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3_3kQU" id="4WAdMLdio1t" role="3_3kQL">
                  <property role="3_3kQV" value=")" />
                </node>
              </node>
              <node concept="2G3XJi" id="4WAdMLdiwsD" role="2G3XIn">
                <property role="1gZaPE" value="" />
                <node concept="1gZcZf" id="4WAdMLdiwWv" role="2G3XIn">
                  <property role="1gZaPE" value="" />
                  <node concept="3_3kQU" id="4WAdMLdiwWw" role="3_3kQL">
                    <property role="3_3kQV" value="plot_" />
                  </node>
                  <node concept="3_3kQU" id="4WAdMLdiwWx" role="3_3kQL">
                    <property role="3_3kQV" value="id" />
                    <node concept="17Uvod" id="4WAdMLdiwWy" role="lGtFl">
                      <property role="2qtEX9" value="text" />
                      <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                      <node concept="3zFVjK" id="4WAdMLdiwWz" role="3zH0cK">
                        <node concept="3clFbS" id="4WAdMLdiwW$" role="2VODD2">
                          <node concept="3clFbF" id="4WAdMLdiwW_" role="3cqZAp">
                            <node concept="2OqwBi" id="4WAdMLdiwWA" role="3clFbG">
                              <node concept="30H73N" id="4WAdMLdiwWB" role="2Oq$k0" />
                              <node concept="3TrcHB" id="4WAdMLdiwWC" role="2OqNvi">
                                <ref role="3TsBF5" to="jrxw:7LvyiX4miiD" resolve="id" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3_3kQU" id="4WAdMLdiwWD" role="3_3kQL">
                    <property role="3_3kQV" value="(" />
                  </node>
                  <node concept="3_3kQU" id="4WAdMLdiwWE" role="3_3kQL">
                    <property role="3_3kQV" value="table" />
                    <node concept="17Uvod" id="4WAdMLdiwWF" role="lGtFl">
                      <property role="2qtEX9" value="text" />
                      <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
                      <node concept="3zFVjK" id="4WAdMLdiwWG" role="3zH0cK">
                        <node concept="3clFbS" id="4WAdMLdiwWH" role="2VODD2">
                          <node concept="3clFbF" id="5c0KNUygUmW" role="3cqZAp">
                            <node concept="2YIFZM" id="5c0KNUygUmX" role="3clFbG">
                              <ref role="1Pybhc" to="1mjk:ESqoaSL1tG" resolve="NameHelper" />
                              <ref role="37wK5l" to="1mjk:ESqoaSL1vt" resolve="RName" />
                              <node concept="2OqwBi" id="5c0KNUygUmY" role="37wK5m">
                                <node concept="2OqwBi" id="5c0KNUygUmZ" role="2Oq$k0">
                                  <node concept="2OqwBi" id="5c0KNUygUn0" role="2Oq$k0">
                                    <node concept="30H73N" id="5c0KNUygUn1" role="2Oq$k0" />
                                    <node concept="3TrEf2" id="5c0KNUygVFn" role="2OqNvi">
                                      <ref role="3Tt5mk" to="6vq7:4QWKC3BlXid" />
                                    </node>
                                  </node>
                                  <node concept="3TrEf2" id="5c0KNUygUn2" role="2OqNvi">
                                    <ref role="3Tt5mk" to="jrxw:3R5AwWRY9K7" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="5c0KNUygUn3" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3_3kQU" id="4WAdMLdiwWR" role="3_3kQL">
                    <property role="3_3kQV" value=")" />
                  </node>
                  <node concept="3_3kQU" id="4WAdMLdiwWS" role="3_3kQL">
                    <property role="3_3kQV" value="" />
                  </node>
                </node>
                <node concept="1gZcZf" id="4WAdMLdiwsE" role="2G3XIn">
                  <property role="1gZaPE" value="ignore &lt;- dev.off()" />
                </node>
                <node concept="3_3kQU" id="4WAdMLdiwsH" role="3_3kQL">
                  <property role="3_3kQV" value="" />
                </node>
              </node>
              <node concept="3_3kQU" id="2AV3DmgQAGw" role="3_3kQL">
                <property role="3_3kQV" value="" />
              </node>
            </node>
            <node concept="17Uvod" id="2AV3DmgQIC5" role="lGtFl">
              <property role="2qtEX9" value="nodeId" />
              <property role="P4ACc" value="32f503e8-061b-451e-bcb0-fef56aa05eb9/8969925079115431553/8969925079115431616" />
              <node concept="3zFVjK" id="2AV3DmgQIC6" role="3zH0cK">
                <node concept="3clFbS" id="2AV3DmgQIC7" role="2VODD2">
                  <node concept="3clFbF" id="2AV3DmgQIY1" role="3cqZAp">
                    <node concept="2OqwBi" id="2AV3DmgQJ3h" role="3clFbG">
                      <node concept="30H73N" id="2AV3DmgQIY0" role="2Oq$k0" />
                      <node concept="2qgKlT" id="2AV3DmgQJrE" role="2OqNvi">
                        <ref role="37wK5l" to="v8sa:2AV3DmgHDX5" resolve="id" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="17Uvod" id="7c9ftft7dwo" role="lGtFl">
              <property role="P4ACc" value="32f503e8-061b-451e-bcb0-fef56aa05eb9/8969925079115431553/2742007948304393655" />
              <property role="2qtEX9" value="enabled" />
              <node concept="3zFVjK" id="7c9ftft7dwp" role="3zH0cK">
                <node concept="3clFbS" id="7c9ftft7dwq" role="2VODD2">
                  <node concept="3clFbF" id="7c9ftft7dLB" role="3cqZAp">
                    <node concept="2OqwBi" id="7c9ftft7dLC" role="3clFbG">
                      <node concept="30H73N" id="7c9ftft7dLD" role="2Oq$k0" />
                      <node concept="2qgKlT" id="7c9ftft7dLE" role="2OqNvi">
                        <ref role="37wK5l" to="v8sa:2od$re20jdz" resolve="errorCatchingEnabled" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1gZcZf" id="5c0KNUygJNF" role="2G3XIn">
            <node concept="3_3kQU" id="5c0KNUygJNG" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
          </node>
          <node concept="1gZcZf" id="5c0KNUygJNl" role="2G3XIn">
            <property role="1gZaPE" value="" />
            <node concept="3_3kQU" id="5c0KNUygJNm" role="3_3kQL">
              <property role="3_3kQV" value="" />
            </node>
          </node>
          <node concept="3_3kQU" id="5c0KNUygJNo" role="3_3kQL">
            <property role="3_3kQV" value="" />
          </node>
          <node concept="raruj" id="5c0KNUygJNQ" role="lGtFl" />
        </node>
        <node concept="3_3kQU" id="5c0KNUygJMC" role="3_3kQL">
          <property role="3_3kQV" value="" />
        </node>
      </node>
      <node concept="3_3kQU" id="5c0KNUygobX" role="3_3kQL">
        <property role="3_3kQV" value="" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="7c9ftft7oDq">
    <property role="TrG5h" value="reduce_Simplest_Heatmap2" />
    <ref role="3gUMe" to="6vq7:4QWKC3BlN_S" resolve="Heatmap2" />
    <node concept="2G3XJi" id="7c9ftft7oDr" role="13RCb5">
      <property role="1gZaPE" value="" />
      <node concept="2G3XJi" id="7c9ftft7qHA" role="2G3XIn">
        <property role="1gZaPE" value="" />
        <node concept="1gZcZf" id="7c9ftft7qHB" role="2G3XIn" />
        <node concept="1gZcZf" id="7c9ftft7qHO" role="2G3XIn">
          <property role="1gZaPE" value="" />
          <node concept="3_3kQU" id="7c9ftft7qL8" role="3_3kQL">
            <property role="3_3kQV" value="heatmap.2(x=as.matrix(" />
          </node>
          <node concept="3_3kQU" id="7c9ftft7qL9" role="3_3kQL">
            <property role="3_3kQV" value="table" />
            <node concept="17Uvod" id="7c9ftft7qNO" role="lGtFl">
              <property role="P4ACc" value="901f5cf3-dc77-4c1e-bc5a-6382baee28b4/1680136183140337486/1680136183140337487" />
              <property role="2qtEX9" value="text" />
              <node concept="3zFVjK" id="7c9ftft7qNP" role="3zH0cK">
                <node concept="3clFbS" id="7c9ftft7qNQ" role="2VODD2">
                  <node concept="3clFbF" id="7c9ftft7w1D" role="3cqZAp">
                    <node concept="2YIFZM" id="7c9ftft7wgf" role="3clFbG">
                      <ref role="37wK5l" to="1mjk:ESqoaSL1vt" resolve="RName" />
                      <ref role="1Pybhc" to="1mjk:ESqoaSL1tG" resolve="NameHelper" />
                      <node concept="2OqwBi" id="7c9ftft99dd" role="37wK5m">
                        <node concept="2OqwBi" id="7c9ftft92JP" role="2Oq$k0">
                          <node concept="2OqwBi" id="7c9ftft7sF_" role="2Oq$k0">
                            <node concept="30H73N" id="7c9ftft7sAK" role="2Oq$k0" />
                            <node concept="3TrEf2" id="7c9ftft98vB" role="2OqNvi">
                              <ref role="3Tt5mk" to="6vq7:4QWKC3BlXid" />
                            </node>
                          </node>
                          <node concept="3TrEf2" id="7c9ftft98XE" role="2OqNvi">
                            <ref role="3Tt5mk" to="jrxw:3R5AwWRY9K7" />
                          </node>
                        </node>
                        <node concept="3TrcHB" id="7c9ftft99_k" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3_3kQU" id="7c9ftft7qLa" role="3_3kQL">
            <property role="3_3kQV" value="))" />
          </node>
          <node concept="3_3kQU" id="7c9ftft7qHW" role="3_3kQL">
            <property role="3_3kQV" value="" />
          </node>
          <node concept="3_3kQU" id="7c9ftft7qHP" role="3_3kQL">
            <property role="3_3kQV" value="" />
          </node>
        </node>
        <node concept="1gZcZf" id="7c9ftft7qHE" role="2G3XIn">
          <property role="1gZaPE" value="" />
          <node concept="3_3kQU" id="7c9ftft7qHF" role="3_3kQL">
            <property role="3_3kQV" value="" />
          </node>
        </node>
        <node concept="3_3kQU" id="7c9ftft7qHH" role="3_3kQL">
          <property role="3_3kQV" value="" />
        </node>
        <node concept="raruj" id="7c9ftft7qIb" role="lGtFl" />
      </node>
      <node concept="3_3kQU" id="7c9ftft7oFD" role="3_3kQL">
        <property role="3_3kQV" value="" />
      </node>
    </node>
  </node>
</model>

